require 'net/http'
require 'uri'
require 'optparse'
require 'digest'
require 'json'
require 'diffy'
require 'open3'
require 'pp'

$url_root="https://rubygems.org"
$global_tag_template="%%global %s %s\n"
$name_tag_template="Name:\t\t\trubygem-%s\n"
$version_tag_template="Version:\t\t%s\n"
$release_tag_template="Release:\t\t%d\n"
$summary_tag_template="Summary:\t\t%s\n"
$license_tag_template="License:\t\t%s\n"
$home_tag_template="URL:\t\t\t%s\n"
$source_tag_template="Source0:\t\t%s\n"
$buildreq_tag_template="BuildRequires:\t%s\n"
$Requires_tag_template="Requires:\t\t%s\n"
$buildarch_tag_template="BuildArch:\t\t%s\n"
$description_tag_template="%%description\n%s\n"
$req_tag_template="Requires:\t\t%s\n"

class RubyPoter
  #元数据类
  def initialize(pkg)
    @url_template="https://rubygems.org/api/v1/gems/"+pkg+".json"
    @url_template2="https://rubygems.org/api/v1/versions/"+pkg+".json"
    resp = Net::HTTP.get(URI(@url_template))
    # resp=File::open(pkg+".json").read
    begin
      @json=JSON.parse(resp)
    rescue Exception =>e
      $logfile.syswrite(pkg+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n")
      $errorfile.syswrite(pkg+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n")
      puts $!.to_s
      @exist=false
      return
    end
    # @json=Nokogiri::HTML(File.open(pkg+".txt","r"))
    if @json != nil
      @module_name=@json["name"]
      @spec_name="rubygem-"+@module_name
      @version=@json["version"]
      @pkg_name=@module_name+"-"+@version+".gem"
      @release=1
      @license=@json["licenses"]
      @desc=@json["info"]
      @summary=""
      @exist=true
    else
      @exist=false
    end
  end

  def getExist
    return @exist
  end

  def getName
    return @module_name
  end

  def getSpecName
    return @spec_name
  end

  def getPkgName
    return @pkg_name
  end

  def getVersion
    return @version
  end

  def getRelease
    return @release
  end

  def getSummary
    if @summary == ""
      resp = Net::HTTP.get(URI(@url_template2))
      @json2=JSON.parse(resp)
      @json2.each do |v|
        if v["number"]==@version
          @summary=v["summary"]
        end
      end
    end
    return @summary
  end

  def getDesc
    @desc.to_s.gsub!("\n\n","\n")
    return @desc
  end

  def getLicense
    return @license
  end

  def getURL
    return url=@json["homepage_uri"]
  end

  #只获取到source0，source1等地址还未获取（关联github）
  def getSource
    return source=@json["gem_uri"]
  end

  def getRequires
    #编译依赖
    requires=Hash.new
    reqs=@json["dependencies"]["development"].each do |r|
      requires[r["name"]]=r["requirements"]
    end
    return requires
  end

  def getRuntimeRequires
    #运行依赖
    requires=Hash.new
    reqs=@json["dependencies"]["runtime"].each do |r|
      requires[r["name"]]=r["requirements"]
      end
    return requires
  end

  def getBuildArch
    return "noarch"
    #  暂时未解决
  end

  def getSHA256
    return sha256=@json["sha"]
  end

end

def prepareBuildEnv(rootpath)
#  准备rpmbuild的环境（目录结构）
  if !argVeri(rootpath:rootpath)
    return
  end
  buildroot=File::join(rootpath,"rpmbuild")
  if not File::directory?(buildroot)
    Dir::mkdir(buildroot)
  end

  for sdir in ['SPECS', 'BUILD', 'SOURCES', 'SRPMS', 'RPMS', 'BUILDROOT'] do
    spath=File::join(buildroot,sdir)
    if not File::directory?(spath)
      Dir::mkdir(spath)
    end
  end
  puts "build rpm environment\n"
  $logfile.syswrite("build rpm environment\n")
  return buildroot
end

def downloadSource(poter,output)
#  下载源码包
  if !argVeri(output:output)
    return false
  end
  source=poter.getSource
  if source == nil
    puts "does not find source\n"
    return false
  end
  path=File::join(output,poter.getPkgName)
  if File::exist?(path)
  sha256_cur = Digest::SHA256.hexdigest(File.open(path, 'rb'){|fs|fs.read})
  sha256_org=poter.getSHA256
  # puts sha256_cur
  # puts sha256_org
    if sha256_cur==sha256_org
      puts format("same source file sha256: %s,skip\n",sha256_cur)
      return
    else
      File::delete(path)
    end
  end
  arg='wget "'+source+'" -P "'+output+'"'
  begin
    stdin, stdout, stderr, wait_thr=Open3.popen3(arg)
    $logfile.syswrite("pkgname:"+poter.getName)
    while line=stdout.gets
      puts line
      $logfile.syswrite(line)
    end
  rescue Exception=> e
    $logfile.syswrite(poter.getName+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $errorfile.syswrite(poter.getName+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $failpkgfile.syswrite(poter.getName)
    exit(1)
    return false
  end
  return true
  # if res
  #   puts format("download source in %s\n",source)
  # end
  # puts format("download source status: %s\n",res)
  # return res
end

def tryInstallReq(pkg)
#  尝试安装依赖包

end

def prepareDepencies(reqs)
  #解决依赖问题
  # if (try_gem_install_package(reqs)==false )
  #   return reqs
  # end
  return ""
end

def prepareInstall(spec)
    #spec中%install部分
    spec.syswrite("if [-d .%{_bindir}]; then\n")
    spec.syswrite("\tmkdir %{buildroot}%{_bindir}\n")
    spec.syswrite("\tcp -a .%{_bindir}/* %{buildroot}%{_bindir}\n")
    spec.syswrite("fi\n")

    spec.syswrite("pushd %{buildroot}\n")
    spec.syswrite("touch filelist.lst\n")
    spec.syswrite("if [-d %{buildroot}%{_bindir}]; then\n")
    spec.syswrite("\tfind .%{_bindir} -type f -printf \"/%h/%f\\n\" >> filelist.lst\n")
    spec.syswrite("fi\n")
    spec.syswrite("popd\n")
    spec.syswrite("mv %{buildroot}/filelist.lst .\n")

end

def osc_package(specfile,rootpath,pkg)
  #执行oscbuild命令
  # 参数：rootpath：根目录
  #      specfile：spec文件地址
  if !argVeri(rootpath:rootpath)
    return false
  end
  puts arg='osc build '+specfile+' --root='+rootpath
  begin
    stdin, stdout, stderr, wait_thr=Open3.popen3(arg)
    res=log(stdin, stdout, stderr, wait_thr,pkg)
  rescue Exception=> e
    $logfile.syswrite(pkg+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $errorfile.syswrite(pkg+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $failpkgfile.syswrite(pkg+"\n")
    puts pkg+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message
    exit(1)
  end
  return res
end

def build_package(specfile,rootpath,pkg)
  #执行rpmbuild命令
  # 参数：rootpath：根目录
  #      specfile：spec文件地址
  if !argVeri(rootpath:rootpath)
    return false
  end
  puts arg='rpmbuild -ba "'+specfile+'"'
  begin
    stdin, stdout, stderr, wait_thr=Open3.popen3(arg)
    res=log(stdin, stdout, stderr, wait_thr,pkg)
  rescue Exception=> e
    $logfile.syswrite(pkg+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $errorfile.syswrite(pkg+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $failpkgfile.syswrite(pkg)
    exit(1)
  end
  return res
end

def veri_provides(diffpath,newfile,oldfile)
  #生成区别provides
  # 参数：
  # diffpath：生成的区别文件的地址
  # newfile：脚本生成的包的provides文件
  # oldfile：rpm包的provides文件
  res=Diffy::Diff.new(newfile,oldfile,:source => 'files').to_s
  if res == ""
    return true
  end
  difffile=File::new(diffpath,"w")
  difffile.syswrite(res)
  return false
end

def veri_package(poter,buildtype,rootpath,veripath,outpath)
  #生成包的provides文件
  # 未输出到日志
  if !argVeri(rootpath: rootpath,verifile: veripath,output: outpath)
    return false
  end
  outdir=File::join(outpath,"diff")
  if !File::directory?outdir
    system('mkdir '+outdir)
  end
  newpath=File::join(outdir,poter.getSpecName+'_new.list')
  oldpath=File::join(outdir,poter.getSpecName+'_old.list')
  diffpath=File::join(outdir,poter.getSpecName+'_diff.list')
  if buildtype == "rpmbuild"
    packpath=File::join(rootpath,"rpmbuild","RPMS",poter.getBuildArch,poter.getSpecName+"-"+poter.getVersion+"*")
    resnew=system('rpm --provides -qpl '+packpath+' > '+newpath)
    resold=system('rpm --provides -qpl '+veripath+'/* > '+oldpath)

  elsif buildtype == "oscbuild"
    packpath=File::join(rootpath,"home","abuild","rpmbuild","RPMS",poter.getBuildArch,poter.getSpecName+"*")
    resnew=system('rpm --provides -qpl '+packpath+' > '+newpath)
    resold=system('rpm --provides -qpl '+veripath+'/* > '+oldpath)
  end
  if !resnew
    puts "build new package provides file failed\n"
    return resnew
  end
  if !resold
    puts "build old package provides file failed\n"
    return resold
  end

  res=veri_provides(diffpath,newpath,oldpath)
  return res
end

def buildOsc(poter,outpath,rootpath)
#  oscbuild功能
# 参数：rootpath：根目录
#      specfile：spec文件地址
  if !argVeri(output: outpath,rootpath:rootpath)
    return false
  end
  res=buildSpec(poter,outpath)
  if !res
    puts "build spec failed\n"
    return res
  end
  puts format("spec file in: %s\n",outpath)
  reqs=poter.getRequires
  ret=prepareDepencies(reqs)
  if ret !=""
    print format("%s can not be installed automatically, Please handle it" ,ret)
    return false
  end
  downloadSource(poter,outpath)
  specfile=File::join(outpath,poter.getSpecName+".spec")
  res=osc_package(specfile,rootpath,poter.getName)
  return res
end

def buildRpm(poter,rootpath)
  #  rpmbuild功能
  # 参数：rootpath：根目录
  #      specfile：spec文件地址
  if !argVeri(rootpath:rootpath)
    return false
  end
  buildroot=prepareBuildEnv(rootpath)
  specdir=File::join(buildroot,"SPECS" )
  res=buildSpec(poter,specdir)
  if !res
    puts "build spec failed\n"
    return res
  end
  puts format("spec file in: %s\n",specdir)
  reqs=poter.getRequires
  ret=prepareDepencies(reqs)
  if ret !=""
    puts format("%s can not be installed automatically, Please handle it" ,ret)
    return false
  end
  sourcedir=File::join(buildroot,"SOURCES")
  downloadSource(poter,sourcedir)
  specfile=File::join(specdir,poter.getSpecName+".spec")
  root=File::join(rootpath,"rpmbuild/")
  res=build_package(specfile,root,poter.getName)
  return res
end

def buildInstallRPM(poter,rootpath,outpath,buildtype)
  #build&Install功能
  # 生成spec文件、build rpm包、安装卸载验证
  # 参数：
  # rootpath：build命令根目录
  # outpath：生成spec文件和源码包下载目录
  # buildtyp：build的类型 "oscbuild"或"rpmbuild"
  if !argVeri(rootpath:rootpath,output: outpath)
    return false
  end
  arch=poter.getBuildArch
  if buildtype == "rpmbuild"
    res=buildRpm(poter,rootpath)
    # pkgname=File::join(rootpath,"rpmbuild","RPMS",arch,poter.getSpecName+"*")
    installdir=File::join(rootpath,"rpmbuild","RPMS",arch)
  elsif buildtype=="oscbuild"
    res=buildOsc(poter,outpath,rootpath)
    # pkgname=File::join(rootpath,"home","abuild","rpmbuild","RPMS",arch,poter.getSpecName+"*")
    installdir=File::join(rootpath,"home","abuild","rpmbuild","RPMS",arch)
  end

  if !res
    puts "build failed\n"
    return res
  end
  files=""
  installfile=Dir::entries(installdir)
  installfile.each do |f|
    if f.include?poter.getSpecName.to_s
      files=files+File::join(installdir,f)+" "
    end
  end
  # res=system('rpm -ivh '+files)
  arg='rpm -ivh '+files
  begin
    stdin, stdout, stderr, wait_thr=Open3.popen3(arg)
    res=log(stdin, stdout, stderr, wait_thr,poter.getName)
  rescue Exception=> e
    $logfile.syswrite(poter.getName+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $errorfile.syswrite(poter.getName+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $failpkgfile.syswrite(poter.getName)
    puts "Install failed\n"
    exit(1)
  end
  if !res
    puts "Install failed\n"
  end

  files=""
  installfile.each do |f|
    if f.include?poter.getSpecName.to_s
      f=f.gsub(".rpm","")
      files=files+f+" "
    end
  end
  # res=system('rpm -e '+files)
  arg='rpm -e '+files
  begin
    stdin, stdout, stderr, wait_thr=Open3.popen3(arg)
    res=log(stdin, stdout, stderr, wait_thr,poter.getName)
  rescue Exception=> e
    $logfile.syswrite(poter.getName+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $errorfile.syswrite(poter.getName+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
    $failpkgfile.syswrite(poter.getName)
    res=false
  end
  if !res
    puts "Uninstall failed\n"
  end
  return res
end

def veriPackage(poter,buildtype,rootpath,veripath,outpath)
  #检查包provides功能
  # 参数：
  # rootpath：build命令根目录
  # outpath：生成spec文件和源码包下载目录
  # buildtyp：build的类型 "oscbuild"或"rpmbuild"

  #g
  if !argVeri(rootpath:rootpath,verifile: veripath,output: outpath)
    return false
  end
  veri_package(poter,buildtype,rootpath,veripath,outpath)
end

def buildSpec(poter,output)
#  生成spec文件
# 参数：
# output：spec文件生成目录
  if !argVeri(output:output)
    return false
  end
  if File::directory? (output)
    spec=File.new(File::join(output,poter.getSpecName+".spec"),"w")
  elsif  File::file?(output)
    spec=File.open(output,"w")
  end
  spec.syswrite(format($global_tag_template,"_empty_manifest_terminate_build",0))
  spec.syswrite(format($global_tag_template,"gem_name",poter.getName))
  spec.syswrite(format($name_tag_template,poter.getName))
  spec.syswrite(format($version_tag_template,poter.getVersion))
  spec.syswrite(format($release_tag_template,1))
  license=""
  if poter.getLicense!=nil
    poter.getLicense.each do |l|
      license=license+l+" "
    end
  end
  spec.syswrite(format($license_tag_template,license))
  spec.syswrite(format($summary_tag_template,poter.getSummary))
  spec.syswrite(format($home_tag_template,poter.getURL))
  spec.syswrite(format($source_tag_template,poter.getSource))
  # reqs=poter.getRequires
  spec.syswrite(format($buildreq_tag_template,"ruby(release)"))
  spec.syswrite(format($buildreq_tag_template,"ruby-devel"))
  spec.syswrite(format($buildreq_tag_template,"rubygems"))
  spec.syswrite(format($buildreq_tag_template,"rubygems-devel"))
  spec.syswrite(format($buildreq_tag_template,"ruby"))
  if poter.getRequires !=nil
    reqs=poter.getRequires.each do |k,v|
      spec.syswrite(format($buildreq_tag_template,"rubygem("+k+")"))
      # spec.syswrite(format($buildreq_tag_template,"rubygem-"+k))
    end
  end
  if poter.getRuntimeRequires !=nil
    reqs=poter.getRuntimeRequires.each do |k,v|
      spec.syswrite(format($Requires_tag_template,"rubygem("+k+")"))
      # spec.syswrite(format($buildreq_tag_template,"rubygem-"+k))
    end
  end
  spec.syswrite(format($buildarch_tag_template,poter.getBuildArch))
  spec.syswrite(format($description_tag_template,poter.getDesc))
  spec.syswrite("\n")
  spec.syswrite("%package doc\n")
  spec.syswrite(format($summary_tag_template,"Documentation for %{name}"))
  spec.syswrite(format($req_tag_template,"%{name} = %{version}-%{release}"))
  spec.syswrite(format($buildarch_tag_template,poter.getBuildArch))
  spec.syswrite("%description doc\n")
  spec.syswrite("Documentation for %{name}.\n")
  spec.syswrite("\n")
  spec.syswrite("%prep\n")
  spec.syswrite("%setup -q -n %{gem_name}-%{version}\n")
  spec.syswrite("\n")
  spec.syswrite("%build\n")
  # spec.syswrite("gem build %{gem_name}.gemspec\n")
  spec.syswrite("%gem_install -n %{SOURCE0}\n")
  spec.syswrite("\n")
  spec.syswrite("%install\n")
  spec.syswrite("mkdir -p %{buildroot}%{gem_dir}\n")
  spec.syswrite("cp -a .%{gem_dir}/* \\\n")
  spec.syswrite("\t%{buildroot}%{gem_dir}/\n")
  prepareInstall(spec)
  # spec.syswrite("pushd %{buildroot}\n")
  # spec.syswrite("if [ -d .%{gem_instdir} ]; then\n")
  # spec.syswrite("\tfind .%{gem_instdir} -maxdepth 1 -type f ! -name \"*LICENSE\" -printf \"%{gem_instdir}/%f\\n\" >>doclist.lst\n")
  # spec.syswrite("fi\n")
  # spec.syswrite("popd\n")
  # spec.syswrite("mv %{buildroot}/doclist.lst .\n")
  spec.syswrite("\n")
  spec.syswrite("%check\n")
  spec.syswrite("# If you need to test, please fill in")
  spec.syswrite("\n")
  spec.syswrite("%files -f filelist.lst\n")
  spec.syswrite("%dir %{gem_instdir}\n")
  # spec.syswrite("%exclude %{gem_instdir}/.*\n")
  spec.syswrite("%license %{gem_instdir}/*LICENSE*\n")
  spec.syswrite("%{gem_libdir}\n")
  spec.syswrite("%exclude %{gem_cache}\n")
  spec.syswrite("%{gem_spec}\n")
  spec.syswrite("\n")
  spec.syswrite("%files doc\n")
  spec.syswrite("%doc %{gem_docdir}\n")
  spec.syswrite("%{gem_instdir}/*\n")
  spec.syswrite("%exclude %{gem_instdir}/lib\n")
  spec.syswrite("%exclude %{gem_instdir}/.*\n")
  # spec.syswrite("%doc %{gem_instdir}/*.md\n")
  # spec.syswrite("%doc %{gem_instdir}/*.txt\n")
  # spec.syswrite("%doc %{gem_instdir}/*.rdoc\n")
  # spec.syswrite("%{gem_instdir}/Gemfile.lock\n")

  spec.syswrite("\n")
  spec.syswrite("%changelog\n")
  time=Time.new
  spec.syswrite(format("* %s geyanan <geyanan2@huawei.com> %s-%s\n",time.strftime("%a %b %d %Y"),poter.getVersion,poter.getRelease))
  spec.syswrite("- package init\n")
  res=File::file?(spec.path)
  if res
    puts format("build spec in %s\n",spec.path)
  end
  puts format("build spec status: %s\n",res)
  if poter.getLicense==nil
    $errorfile.syswrite(poter.getName+"\nexception info:\n""[Error] Empty License!\n")
    $logfile.syswrite(poter.getName+"\nexception info:\n""[Error] Empty License!\n")
    # raise StandardError.new("[Error] Empty License!\n")
    return false
  end
  return res
end

def batch(expspath,output,rootpath,veripath)
  #批处理功能
  # rootpath：build命令根目录
  # outpath：生成spec文件和源码包下载目录
  # veripath：对比的rpm包的路径
  if !argVeri(output: output,rootpath:rootpath,verifile: veripath)
    return false
  end
  if expspath == nil
    puts "Invalid expspath,please check or [-a] option\n"
    return false
  end

  logdir=File::join(output,"logs")
  if !File::directory?logdir
    res=Dir::mkdir(logdir)
  end
  expsfile=File::open(expspath,"r+")
  logfile=File::open(File::join(logdir,"log"+Time.now.to_i.to_s+".txt"),"w+")
  errorfile=File::open(File::join(logdir,"error"+Time.now.to_i.to_s+".txt"),"w+")
  failpkgfile=File::new(File::join(logdir,"failPkg.txt"),"w+")
  sucpkgfile=File::new(File::join(logdir,"sucPkg.txt"),"w+")

  expsfile.readlines.each do |f|
    begin
      # res=system(format("ruby rubypoter.rb -B -c -v %s -o %s -r %s -t %s %s",$verify,$output,$rootpath,$type,f.to_s))
      Open3.popen3(format("ruby rubypoter.rb -B -c -o %s -r %s -t %s %s",output,rootpath,"ruby",f.to_s.gsub("\n",""))){ |stdin, stdout, stderr, wait_thr|
        logfile.syswrite("pkgname:"+f.to_s)
        while line=stdout.gets
          puts line
          logfile.syswrite(line)
        end
        error=stderr.read
        error=error.to_s.gsub("WARNING: SSL certificate checks disabled. Connection is insecure!","").gsub(/^$\n/, '')
        if error != ""
          errorfile.syswrite("pkgname:"+f.to_s+"exception info:\n"+error+"\n")
          logfile.syswrite("error:\n"+error+"\n")
          failpkgfile.syswrite(f)
          puts error
        else
          sucpkgfile.syswrite(f)
        end

      }
    rescue  Exception=> e
      logfile.syswrite(f.to_s+"\nexception location:"+$@.to_s+"\nexception info:\n"+$!.to_s+"\n"+e.message)
      failpkgfile.syswrite(f)
      return false
    end

    logfile.syswrite('='*60+"\n")
    errorfile.syswrite('='*60+"\n")
  end
  return true
end

def doargs()
  #解析参数
  options={:output=>'/root/home:GYN:branches:openEuler:20.09/auto',:rootpath=>'/root/osc/auto/',
           :type=>'ruby',:path=>'/root/home:GYN:branches:openEuler:20.09/auto',
           :verifile=>'/root/home:GYN:branches:openEuler:20.09/auto',:batchfile=>'/root/home:GYN:branches:openEuler:20.09/auto/1.txt'}
  option_parser=OptionParser.new do |opts|
    opts.banner='here is help messages of the command line tool.'

    opts.on('-s','--spec','Create spec file') do
      options[:spec]=true
    end

    opts.on('-b','--build','RPMBuild file') do
      options[:build]=true
    end

    opts.on('-c','--build','osc build file') do
      options[:osc]=true
    end

    opts.on('-R','--requires','Get required python modules') do
      options[:requires]=true
    end

    opts.on('-B','--buildinstal','Build&Install rpm package') do
      options[:buildinstall]=true
    end

    opts.on('-d','--download','Download source file indicated path') do
      options[:download]=true
    end

    opts.on('-o','--output [Output]','Output to file') do |p|
      if p!=nil
        options[:output]=p
      end

    end

    opts.on('-r','--rootpath [Rootpath]','Build rpm package in root pat') do |p|
      if p!=nil
        options[:rootpath]=p
      end

    end

    opts.on('-p','--path [Path]','indicated path to store files') do |p|
      if p!=nil
        options[:path]=p
      end

    end

    opts.on('-v','--verification [Verification]','Verification files') do |p|
      options[:veri]=true
      if p!=nil
        options[:verifile]=p
      end
    end

    opts.on('-t','--type [Type]','Build module type : python, perl...') do |p|
      if p!=nil
        options[:type]=p
      end
    end

    opts.on('-a','--batch [BatchFile]','batch build & install') do |p|
      options[:batch]=true
      if p!=nil
        options[:batchfile]=p
      end
    end

    opts.on('-h','--help',"prints help info") do
      puts opts
      exit
    end
  end.parse!
  return options
end

def createPoter(type,pkg)
  #创建poter对象
  # 参数：
  # type：包类型  ruby
  # pkg：包名
  if argVeri(type:type)
    return poter=RubyPoter.new(pkg)
  end
end

def poter(parse,pkg)
  #主程序函数
  poter=createPoter(parse[:type],pkg)
  puts parse

  if poter == nil or !poter.getExist
    puts "Type or Package Name is not supported now\n"
    return false
    # exit(1)
  end

  if parse[:requires]
    reqs=poter.getRequires
    if reqs == nil
      puts "No Building Requires\n"
    else
      reqs.each do |k,v|
        puts format("%s %s needed by %s\n",k,v,poter.getName)
      end
    end

  elsif parse[:buildinstall]
    if parse[:build]
      buildtype="rpmbuild"
    elsif parse[:osc]
      buildtype="oscbuild"
    end
    res=buildInstallRPM(poter,parse[:rootpath],parse[:output],buildtype)
    if !res
      puts format("build & install status: %s\n",res)
      # exit(1)
      return false
    end

  elsif parse[:build]
    res=buildRpm(poter,parse[:rootpath])
    if !res
      puts format("rpm build status: %s\n",res)
      return false
      # exit(1)
    end

  elsif parse[:osc]
    res=buildOsc(poter,parse[:output],parse[:rootpath])
    if !res
      puts format("osc build status: %s\n",res)
      return false
      # exit(1)
    end

  elsif parse[:spec]
    res=buildSpec(poter,parse[:output])
    if !res
      puts format("build spec status: %s\n",res)
      return false
    end

  elsif parse[:download]
    res=downloadSource(poter,parse[:path])
    if res
      puts format("download source file in %s\n",parse[:path])
    else
      puts "download souce file failed\n"
      return false
    end
  end

  if parse[:veri]
    if parse[:build]
      buildtype="rpmbuild"
    elsif parse[:osc]
      buildtype="oscbuild"
    end
    res=veriPackage(poter,buildtype,parse[:rootpath],parse[:verifile],parse[:output])
    if !res
      puts format("rpm verification status: %s\n",res)
      return false
      # exit(1)
    end
  end
  return true
end

def argVeri(output:"default",rootpath:"default",type:"default",path:"default",verifile:"default",batchfile:"default")
  # 参数验证函数
  res=true
  if output != "default" and !File::directory?output and !File::file?output
    puts "Invalid arguments,please check or [-o] option\n"
    res=false
  end
  if rootpath != "default" and !File::directory?rootpath
    puts "Invalid arguments,please check or [-r] option\n"
    res= false
  end
  if type != "default" and type!="ruby"
    puts "Invalid arguments,please check or [-t] option\n"
    res= false
  end
  if path != "default" and !File::directory?path
    puts "Invalid arguments,please check [-p] option\n"
    res= false
  end
  if verifile != "default" and !File::directory?verifile
    puts "Invalid arguments,please check or [-v] option\n"
    res= false
  end
  if batchfile != "default" and !File::file?batchfile
    puts "Invalid arguments,please check  [-a] option\n"
    res= false
  end
  return res
end

def log(stdin, stdout, stderr, wait_thr,pkg)
#  解析并存储日志函数
# stdin:
# stdout:日志输出流
# stderr:错误日志输出流
# pkg:包名
# wait_thr:
# 返回 true：无错误日志
#     false：存在错误日志
  $logfile.syswrite("pkgname:"+pkg+"\n")
  while line=stdout.gets
    puts line
    $logfile.syswrite(line)
  end
  error=stderr.read
  error=error.to_s.gsub("WARNING: SSL certificate checks disabled. Connection is insecure!","").gsub(/^$\n/, '')
  if error != ""
    $errorfile.syswrite("pkgname:"+pkg+"\nexception info:\n"+error+"\n")
    $logfile.syswrite("error:\n"+error+"\n")
    puts error
    return false
  else
    return true
  end
end



#程序入口
parse=doargs

#日志文件初始化
logdir=File::join(parse[:output],"logs")
if !File::directory?logdir
  res=Dir::mkdir(logdir)
end
$logfile=File::open(File::join(logdir,"log"+Time.now.to_i.to_s+".txt"),"w+")
$errorfile=File::open(File::join(logdir,"error"+Time.now.to_i.to_s+".txt"),"w+")
$failpkgfile=File::new(File::join(logdir,"failPkg.txt"),"w+")
$sucpkgfile=File::new(File::join(logdir,"sucPkg.txt"),"w+")

#批量处理
if parse[:batch]
  expsfile=File::open(parse[:batchfile],"r+")

  expsfile.readlines.each do |f|
    f=f.to_s.gsub!("\n","")
    output=parse[:output]
    parse[:output]=File::join(parse[:output],f)
    if !File::directory?parse[:output]
      Dir::mkdir(parse[:output])
    end
    res=poter(parse,f)
    parse[:output]=output
    if res
      $sucpkgfile.syswrite(f+"\n")
    else
      $failpkgfile.syswrite(f+"\n")
    end
    $logfile.syswrite('='*60+"\n")
    $errorfile.syswrite('='*60+"\n")
  end
else
  #单处理
  $pkg=ARGV[0]
  if $pkg ==nil
    puts "Nothing provided package name\n"
    exit(1)
  end
  poter(parse,$pkg)
  $logfile.syswrite('='*60+"\n")
  $errorfile.syswrite('='*60+"\n")
end


